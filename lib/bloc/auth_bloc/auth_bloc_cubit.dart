import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/ui/login/login_page.dart';
import 'package:path/path.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  void fetch_history_login() async {
    emit(AuthBlocInitialState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool? isLoggedIn = sharedPreferences.getBool("is_logged_in");
    if (isLoggedIn == null) {
      emit(AuthBlocLoginState());
    } else {
      if (isLoggedIn) {
        emit(AuthBlocLoggedInState());
      } else {
        emit(AuthBlocLoginState());
      }
    }
  }

  void move_to_login() {
    emit(AuthBlocLoginState());
  }

  void move_to_register() {
    emit(AuthBlocRegisterState());
  }

  void login_user(User user, context) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String data = user.toJson().toString();
    sharedPreferences.setString("user_value", data);
    var databasePath = await getDatabasesPath();
    String path = join(databasePath, 'database.db');
    Database database = await openDatabase(path, version: 1, onCreate: (Database db, int version) async {
      await db.execute(
          'CREATE TABLE IF NOT EXISTS Users (id INTEGER PRIMARY KEY, username TEXT, email TEXT, password TEXT)');
    }, onOpen: (Database db) async {
      await db.execute(
          'CREATE TABLE IF NOT EXISTS Users (id INTEGER PRIMARY KEY, username TEXT, email TEXT, password TEXT)');
    });
    List<Map> list = await database.rawQuery('SELECT * FROM Users');
    print(list);
    if (list.map((e) => e['email']).contains(user.email) && list.map((e) => e['password']).contains(user.password)) {
      await sharedPreferences.setBool("is_logged_in", true);
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('Login Berhasil'),
        ),
      );
      emit(AuthBlocLoggedInState());
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          backgroundColor: Colors.red,
          content: Text('Login Gagal, periksa kembali inputan anda'),
        ),
      );
    }
  }

  void register(User user, context) async {
    var databasePath = await getDatabasesPath();
    String path = join(databasePath, 'database.db');
    Database database = await openDatabase(path, version: 1, onCreate: (Database db, int version) async {
      await db.execute(
          'CREATE TABLE IF NOT EXISTS Users (id INTEGER PRIMARY KEY, username TEXT, email TEXT, password TEXT)');
    }, onOpen: (Database db) async {
      await db.execute(
          'CREATE TABLE IF NOT EXISTS Users (id INTEGER PRIMARY KEY, username TEXT, email TEXT, password TEXT)');
    });
    List<Map> list = await database.rawQuery('SELECT * FROM Users');
    if (list.isNotEmpty && list.map((e) => e['email']).contains(user.email)) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          backgroundColor: Colors.red,
          content: Text('Email sudah digunakan'),
        ),
      );
    } else {
      await database.transaction((txn) async {
        int id1 = await txn.rawInsert(
            'INSERT INTO Users(username, email, password) VALUES("${user.userName}", "${user.email}", "${user.password}")');
        print('inserted1: $id1');
      });
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('Register berhasil'),
        ),
      );
      emit(AuthBlocLoggedInState());
    }
  }

  void logging_out() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.setBool("is_logged_in", false);
    sharedPreferences.clear();
    emit(AuthBlocInitialState());
    emit(AuthBlocLoginState());
  }
}
