import 'package:flutter/material.dart';

class TextBelowLoginButton extends StatelessWidget {
  const TextBelowLoginButton({
    Key? key,
    required this.context,
    required this.firstText,
    required this.secondText,
    this.onPressed,
  }) : super(key: key);

  final BuildContext context;
  final String firstText;
  final String secondText;
  final Function()? onPressed;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: onPressed ?? null,
        child: RichText(
          text: TextSpan(text: '$firstText ', style: TextStyle(color: Colors.white), children: [
            TextSpan(
              text: '$secondText',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ]),
        ),
      ),
    );
  }
}