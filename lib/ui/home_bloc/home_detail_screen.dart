import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:majootestcase/models/movie_response.dart';

class HomeDetailScreen extends StatefulWidget {
  const HomeDetailScreen({Key? key, this.data}) : super(key: key);

  final Data? data;

  @override
  State<HomeDetailScreen> createState() => _HomeDetailScreenState();
}

class _HomeDetailScreenState extends State<HomeDetailScreen> with SingleTickerProviderStateMixin {
  late TabController tabController;

  @override
  void initState() {
    tabController = TabController(length: 3, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: Text(
          'Movie Detail',
          style: GoogleFonts.sourceSansPro(
            fontWeight: FontWeight.bold,
          ),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: Stack(
        children: [
          Container(
            height: MediaQuery.of(context).size.height,
            child: Image.network(
              widget.data!.i!.imageUrl!,
              fit: BoxFit.cover,
            ),
          ),
          Container(color: Colors.black.withOpacity(.90)),
          SingleChildScrollView(
            child: Center(
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(12),
                      child: SizedBox(
                        width: MediaQuery.of(context).size.width / 2,
                        child: Image.network(widget.data!.i!.imageUrl!),
                      ),
                    ),
                    SizedBox(height: 20),
                    Text(
                      widget.data!.l!,
                      style: GoogleFonts.sourceSansPro(
                        fontWeight: FontWeight.w600,
                        fontSize: 24,
                        color: Colors.white,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: 20),
                    RichText(
                      text: TextSpan(
                        text: '${widget.data!.year}',
                        style: GoogleFonts.sourceSansPro(
                          fontSize: 18,
                          color: Colors.white,
                        ),
                        children: [
                          TextSpan(
                            text: '  ',
                          ),
                          TextSpan(
                            text: '${widget.data!.q}',
                            style: GoogleFonts.sourceSansPro(color: Colors.white70),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 30),
                    if (widget.data!.series != null)
                      StaggeredGridView.countBuilder(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: widget.data!.series!.length,
                        crossAxisCount: 2,
                        crossAxisSpacing: 10,
                        mainAxisSpacing: 20,
                        itemBuilder: (context, index) {
                          return movieDetailItem(context, index);
                        },
                        staggeredTileBuilder: (index) => StaggeredTile.fit(1),
                      ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget movieDetailItem(BuildContext context, int index) {
    return Column(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(12),
          child: SizedBox(
            width: MediaQuery.of(context).size.width / 2,
            child: Image.network(widget.data!.series![index].i!.imageUrl!),
          ),
        ),
        SizedBox(height: 20),
        Text(
          widget.data!.series![index].l!,
          style: GoogleFonts.sourceSansPro(
            fontWeight: FontWeight.w600,
            fontSize: 16,
            color: Colors.white,
          ),
          textAlign: TextAlign.center,
          overflow: TextOverflow.ellipsis,
          maxLines: 1,
        ),
      ],
    );
  }
}
