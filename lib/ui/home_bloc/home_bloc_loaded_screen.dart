import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/models/movie.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/ui/home_bloc/home_detail_screen.dart';

class HomeBlocLoadedScreen extends StatefulWidget {
  final List<Data>? data;

  const HomeBlocLoadedScreen({Key? key, this.data}) : super(key: key);

  @override
  State<HomeBlocLoadedScreen> createState() => _HomeBlocLoadedScreenState();
}

class _HomeBlocLoadedScreenState extends State<HomeBlocLoadedScreen> {
  late PageController pageController;

  void _animateSlider() {
    Future.delayed(Duration(seconds: 3)).then((_) {
      int nextPage = pageController.page!.round() + 1;

      if (nextPage == widget.data!.length) {
        nextPage = 0;
      }

      pageController.animateToPage(nextPage, duration: Duration(seconds: 1), curve: Curves.ease).then(
            (_) => _animateSlider(),
          );
    });
  }

  @override
  void initState() {
    super.initState();
    pageController = PageController();
    _animateSlider();
  }

  @override
  void dispose() {
    super.dispose();
    pageController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: Text(
          'Movie App',
          style: GoogleFonts.sourceSansPro(
            fontWeight: FontWeight.bold,
          ),
        ),
        elevation: 0,
        backgroundColor: Colors.transparent,
        actions: [
          IconButton(
            onPressed: () => context.read<AuthBlocCubit>().logging_out(),
            icon: Icon(Icons.exit_to_app),
            tooltip: 'Logout',
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            movieCarousel(context),
            SizedBox(height: 30),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Text(
                'Popular Now',
                style: GoogleFonts.sourceSansPro(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
              ),
            ),
            SizedBox(height: 5),
            SizedBox(
              height: MediaQuery.of(context).size.width * .90,
              child: ListView.builder(
                padding: EdgeInsets.only(left: 20),
                scrollDirection: Axis.horizontal,
                shrinkWrap: true,
                itemCount: widget.data!.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => HomeDetailScreen(
                            data: widget.data![index],
                          ),
                        ),
                      );
                    },
                    child: movieListContent(context, index),
                  );
                },
              ),
            ),
            SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Text(
                'Game of Thrones',
                style: GoogleFonts.sourceSansPro(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
              ),
            ),
            SizedBox(height: 5),
            seriesContent(context)
          ],
        ),
      ),
    );
  }

  Widget seriesContent(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.width * .50,
      child: ListView.builder(
        padding: const EdgeInsets.only(left: 20),
        scrollDirection: Axis.horizontal,
        shrinkWrap: true,
        itemCount: widget.data![0].series!.length,
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.only(right: 16),
            child: SizedBox(
              height: MediaQuery.of(context).size.width * .40,
              width: MediaQuery.of(context).size.width / 2,
              child: Column(
                children: [
                  ClipRRect(
                    child: Image.network(widget.data![0].series![index].i!.imageUrl!),
                    borderRadius: BorderRadius.circular(15),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      widget.data![0].series![index].l!,
                      style: GoogleFonts.sourceSansPro(
                        fontWeight: FontWeight.w600,
                        color: Colors.white,
                      ),
                      textAlign: TextAlign.center,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Widget movieListContent(BuildContext context, int index) {
    return Padding(
      padding: const EdgeInsets.only(right: 20),
      child: SizedBox(
        width: MediaQuery.of(context).size.width / 2,
        child: Column(
          children: [
            ClipRRect(
              child: SizedBox(
                height: MediaQuery.of(context).size.width * .80,
                width: MediaQuery.of(context).size.width / 2,
                child: Image.network(
                  widget.data![index].i!.imageUrl!,
                  fit: BoxFit.cover,
                ),
              ),
              borderRadius: BorderRadius.circular(16),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                widget.data![index].l!,
                style: GoogleFonts.sourceSansPro(
                  fontWeight: FontWeight.w600,
                  color: Colors.white,
                ),
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget movieCarousel(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.width,
      child: PageView.builder(
        controller: pageController,
        scrollDirection: Axis.horizontal,
        itemCount: widget.data!.length,
        itemBuilder: (context, index) {
          return Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.width,
            child: Image.network(
              widget.data![index].i!.imageUrl!,
              fit: BoxFit.cover,
            ),
          );
        },
      ),
    );
  }
}
