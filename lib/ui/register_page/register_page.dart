import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/text_below_login_button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/ui/extra/loading.dart';

class RegisterPage extends StatefulWidget {
  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  TextController? usernameController;
  TextController? emailController;
  TextController? passwordController;
  TextController? confirmPasswordController;
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;

  @override
  void initState() {
    usernameController = TextController();
    emailController = TextController();
    passwordController = TextController();
    confirmPasswordController = TextController();
    super.initState();
  }

  @override
  void dispose() {
    usernameController!.dispose();
    emailController!.dispose();
    passwordController!.dispose();
    confirmPasswordController!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: BlocConsumer<AuthBlocCubit, AuthBlocState>(
        listener: (context, state) {},
        builder: (context, state) {
          if (state is AuthBlocLoadingState) {
            return LoadingIndicator();
          }
          return SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Selamat Datang',
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold, color: Colors.white,
                      // color: colorBlue,
                    ),
                  ),
                  Text(
                    'Silahkan register terlebih dahulu',
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w400,
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(
                    height: 9,
                  ),
                  _form(),
                  SizedBox(
                    height: 30,
                  ),
                  CustomButton(
                    text: 'Register',
                    onPressed: handleRegister,
                    height: 100,
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  TextBelowLoginButton(
                    context: context,
                    firstText: 'Sudah punya akun?',
                    secondText: 'Login',
                    onPressed: () => context.read<AuthBlocCubit>().move_to_login(),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: usernameController,
            isEmail: true,
            validator: (val) {
              if (val == null) return 'Form tidak boleh kosong, mohon cek kembali data yang anda inputkan';
            },
            hint: 'username',
            label: 'Username',
          ),
          CustomTextFormField(
            context: context,
            controller: emailController,
            isEmail: true,
            hint: 'example@gmail.com',
            label: 'Email',
            validator: (val) {
              final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              if (val != null) return pattern.hasMatch(val) ? null : 'Masukkan e-mail yang valid';
            },
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            validator: (val) {
              if (val == null) return 'Form tidak boleh kosong, mohon cek kembali data yang anda inputkan';
            },
            controller: passwordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword ? Icons.visibility_off_outlined : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
          CustomTextFormField(
            context: context,
            label: 'Confirm Password',
            hint: 'password',
            validator: (String? val) {
              if (val == null) return 'Form tidak boleh kosong, mohon cek kembali data yang anda inputkan';
              if (val != passwordController?.value) return 'Password tidak valid';
            },
            controller: confirmPasswordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword ? Icons.visibility_off_outlined : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  void handleRegister() async {
    final String? _username = usernameController!.value;
    final String? _email = emailController!.value;
    final String? _password = passwordController!.value;
    final String? _confirmPassword = confirmPasswordController!.value;
    if (formKey.currentState?.validate() == true &&
        _username != null &&
        _email != null &&
        _password != null &&
        _confirmPassword != null) {
      User user = User(
        userName: _username,
        email: _email,
        password: _password,
      );
      context.read<AuthBlocCubit>().register(user, context);
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          backgroundColor: Colors.red,
          content: Text('Mohon cek kembali data yang anda inputkan.'),
        ),
      );
    }
  }
}
