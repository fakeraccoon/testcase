import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/text_below_login_button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/ui/extra/loading.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_screen.dart';
import 'package:majootestcase/ui/register_page/register_page.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  late TextController _emailController;
  late TextController _passwordController;
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;

  @override
  void initState() {
    _emailController = TextController();
    _passwordController = TextController();
    super.initState();
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: BlocConsumer<AuthBlocCubit, AuthBlocState>(
        listener: (context, state) {},
        builder: (context, state) {
          if (state is AuthBlocLoadingState) {
            return LoadingIndicator();
          }
          return SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Selamat Datang',
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      // color: colorBlue,
                    ),
                  ),
                  Text(
                    'Silahkan login terlebih dahulu',
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w400,
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(
                    height: 9,
                  ),
                  _form(),
                  SizedBox(
                    height: 30,
                  ),
                  CustomButton(
                    text: 'Login',
                    onPressed: handleLogin,
                    height: 100,
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  TextBelowLoginButton(
                    context: context,
                    firstText: 'Belum punya akun?',
                    secondText: 'Daftar',
                    onPressed: () => context.read<AuthBlocCubit>().move_to_register(),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'example@gmail.com',
            label: 'Email',
            validator: (val) {
              final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              if (val != null) return pattern.hasMatch(val) ? null : 'Masukkan e-mail yang valid';
            },
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            validator: (val) {
              if (val == null) return 'Form tidak boleh kosong, mohon cek kembali data yang anda inputkan';
            },
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword ? Icons.visibility_off_outlined : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  void handleLogin() async {
    final String? _email = _emailController.value;
    final String? _password = _passwordController.value;
    if (formKey.currentState?.validate() == true && _email != null && _password != null) {
      User user = User(
        email: _email,
        password: _password,
      );
      context.read<AuthBlocCubit>().login_user(user, context);
    }
  }
}
